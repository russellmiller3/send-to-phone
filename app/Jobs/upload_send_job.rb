class UploadSendJob

	def initialize(id)
		@id = id
	end

  def perform

  	@message = Message.find(@id)

    uploaded_file = convert_data_uri_to_upload(@message)

    # S3 code
    s3 = Aws::S3::Resource.new(
        credentials: Aws::Credentials.new(ENV['AWS_ACCESS_KEY_ID'],
          ENV['AWS_SECRET_ACCESS_KEY']),
        region: ENV['AWS_REGION']
      )
    obj = s3.bucket('sendtophone').object(@message.id.to_s+".png")
    obj.upload_file(uploaded_file.path, acl:'public-read', content_type:'image/png')
    # End S3 code

    # Twilio code
    require 'twilio-ruby'

    account_sid = Twilio_API[:account_sid]
    auth_token = Twilio_API[:auth_token]

    @client = Twilio::REST::Client.new account_sid, auth_token

    @client.account.messages.create(
      :body => "",
      :to => "+1#{@message[:phone]}",
      :from => "+14153196794",
      :media_url => "#{obj.public_url}"
      )

    # End Twilio Code
  end

  def max_attempts
		1
	end


  # Split up a data uri
  def split_base64(uri_str)
    if uri_str.match(%r{^data:(.*?);(.*?),(.*)$})
      uri = Hash.new
      uri[:type] = $1 # "image/gif"
      uri[:encoder] = $2 # "base64"
      uri[:data] = $3 # data string
      uri[:extension] = $1.split('/')[1] # "gif"
      return uri
    else
      return nil
    end
  end



  # Convert data uri to uploaded file. Expects object hash, eg: params[:post]
  def convert_data_uri_to_upload(obj_hash)
    if obj_hash[:file].try(:match, %r{^data:(.*?);(.*?),(.*)$})
      image_data = split_base64(obj_hash[:file])
      image_data_binary = Base64.decode64(image_data[:data])

      temp_img_file = Tempfile.new("data_uri-upload")
      temp_img_file.binmode
      temp_img_file << image_data_binary
      temp_img_file.rewind

      img_params = {:filename => "data-uri-img.#{image_data[:extension]}", :type => image_data[:type], :tempfile => temp_img_file}
      uploaded_file = ActionDispatch::Http::UploadedFile.new(img_params)

      #obj_hash[:file] = uploaded_file
    end

    return uploaded_file
  end



end

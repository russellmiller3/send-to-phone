class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.string :phone
      t.string :file

      t.timestamps null: false
    end
  end
end

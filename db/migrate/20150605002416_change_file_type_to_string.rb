class ChangeFileTypeToString < ActiveRecord::Migration
  def up
  	change_column :messages, :file, :text
  end

  def down
  	change_column :messages, :file, :string
  end

end

  $('form#modal_feedback').submit(function(e) {
      e.preventDefault();
      html2canvas(document.getElementById("sendtophone"), {
        onrendered: function(canvas) {
          var sendtophone_img = canvas.toDataURL(); // Saves canvas as png file
          var data = { phone: $('input#message_phone').val(), file: sendtophone_img }
          $.ajax({
            url: $('form#modal_feedback').attr('action'),
            method: 'POST' ,
            data: { message: data }
          })
        }
    });
 });

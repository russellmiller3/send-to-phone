  $('form#modal_feedback').submit(function(e) {
      e.preventDefault();
      html2canvas(document.getElementById("sendtophone"), {
        onrendered: function(canvas) {
          var sendtophone_img = canvas.toDataURL(); // Saves canvas as png file
          var data = { phone: $('input#message_phone').val(), file: sendtophone_img }
          $.ajax({
            success: function(){

              var modalWrapper = document.getElementById("modal_wrapper_success");
              var modalWindow  = document.getElementById("modal_window_success");

              var openModal = function()
              {
                modalWrapper.className = "overlay";
                modalWindow.style.marginTop = (-modalWindow.offsetHeight)/2 + "px";
                modalWindow.style.marginLeft = (-modalWindow.offsetWidth)/2 + "px";
              };

              var closeModal = function(e)
              {
                modalWrapper.className = "";
                e.preventDefault();
              };

              var clickHandler = function(e) {
                if(e.target.tagName == "DIV") {
                  if(e.target.id != "modal_window_success") closeModal(e);
                }
              };

              var keyHandler = function(e) {
                if(e.keyCode == 27) closeModal(e);
              };

              openModal();
              document.getElementById("modal_close_success").addEventListener("click", closeModal, false);
              document.addEventListener("click", clickHandler, false);
              document.addEventListener("keydown", keyHandler, false);
            },
            url: $('form#modal_feedback').attr('action'),
            method: 'POST' ,
            data: { message: data }
          })
        }
    });
 });
